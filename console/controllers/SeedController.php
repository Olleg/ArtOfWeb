<?php
/**
 * @author Antonov Oleg <theorder83dev@gmail.com>
 */

namespace console\controllers;

use common\models\Category;
use common\models\Product;
use Faker\Factory;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\Console;
use yii\helpers\FileHelper;

/**
 * Class SeedController
 * @package console\controllers
 */
class SeedController extends Controller
{
    const ROOT_CATEGORY = 3;
    const SUB_CATEGORY = 15;
    const PRODUCT = 100;

    protected $entities = [
        Category::class,
        Product::class,
    ];

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws \Exception
     */
    public function beforeAction($action)
    {
        if (YII_ENV_PROD) {
            throw new \Exception('This action locked in prod.');
        }

        return parent::beforeAction($action);
    }

    /**
     * @return int
     */
    public function actionCategories()
    {
        $faker = (new Factory())->create();

        $count = self::ROOT_CATEGORY + self::SUB_CATEGORY;
        Console::startProgress(0, $count);
        $mt = microtime(true);

        // generate root categories
        for ($i = 0; $i < self::ROOT_CATEGORY; $i++) {
            (new Category([
                'id' => $i + 1,
                'name' => $faker->word,
                'parent_id' => NULL,
            ]))->tryInsert(false);
            Console::updateProgress($i, $count);
        }

        //generate children categories
        for ($i = 0; $i < self::SUB_CATEGORY; $i++) {
            (new Category([
                'id' => $i + self::ROOT_CATEGORY + 1,
                'name' => $faker->word,
                'parent_id' => $faker->numberBetween(1, self::ROOT_CATEGORY),
            ]))->tryInsert(false);
            Console::updateProgress(self::ROOT_CATEGORY + $i, $count);
        }

        $elapsed = microtime(true) - $mt;
        Console::endProgress('inserted ' . $count . ' categories, spend ' . $elapsed . ' seconds, average ' . $elapsed / $count . PHP_EOL);

        return ExitCode::OK;
    }

    /**
     * @return int
     */
    public function actionProducts()
    {
        $faker = (new Factory())->create();

        Console::startProgress(0, self::PRODUCT);
        $mt = microtime(true);
        $path = sys_get_temp_dir();

        FileHelper::createDirectory(\Yii::getAlias('@backend/web/images/product/'));

        // generate products
        for ($i = 0; $i < self::PRODUCT; $i++) {

            $filePath = $faker->image($path, 640, 480);
            $product = new Product([
                'category_id' => $faker->numberBetween(self::ROOT_CATEGORY + 1, self::ROOT_CATEGORY + self::SUB_CATEGORY),
                'name' => $faker->word,
                'type' => $faker->randomElement([Product::TYPE_DEFAULT, Product::TYPE_NEW]),
                'price' => $faker->numberBetween(100, 10000),
                'image' => $filePath,
            ]);
            $product->trySave(true);

            //copy image and create thumb
            rename($filePath, $product->getUploadedFilePath('image'));
            @unlink($filePath);
            $product->createThumbs();

            Console::updateProgress($i, self::PRODUCT);
        }

        $elapsed = microtime(true) - $mt;
        Console::endProgress('inserted ' . self::PRODUCT . ' products, spend ' . $elapsed . ' seconds, average ' . $elapsed / self::PRODUCT . PHP_EOL);

        return ExitCode::OK;
    }

    /**
     * @return int
     */
    public function actionInfo()
    {
        echo "****** Records information ******" . PHP_EOL;
        foreach ($this->entities as $entity) {
            try {
                echo $entity . " - " . $entity::find()->count() . PHP_EOL;
            } catch (\Exception $e) {
                echo $entity . " - could not load information" . PHP_EOL;
            }
        }

        return ExitCode::OK;
    }

    /**
     * @return int
     * @throws \Exception
     */
    public function actionClear()
    {
        $this->actionInfo();

        if (!($this->confirm('Confirm delete?'))) {
            return ExitCode::UNSPECIFIED_ERROR;
        }

        $dbt = \Yii::$app->db->beginTransaction();
        try {
            \Yii::$app->db->createCommand()->checkIntegrity(false)->execute();
            foreach ($this->entities as $entity) {
                try {
                    $entity::deleteAll();
                } catch (\Exception $e) {
                    echo $entity . " - an error occurred, maybe the records were not deleted" . PHP_EOL;
                }
            }
            \Yii::$app->db->createCommand()->checkIntegrity(true)->execute();
            $dbt->commit();
        } catch (\Exception $e) {
            $dbt->rollBack();
            throw $e;
        }

        return ExitCode::OK;
    }

    /**
     * @return int
     */
    public function actionGenerate()
    {
        $this->actionClear();

        $actions = [
            'actionCategories',
            'actionProducts',
        ];

        foreach ($actions as $index => $action) {
            echo $index + 1 . '/' . count($actions) . " {$action} START" . PHP_EOL;
            $this->$action();
            echo $index + 1 . '/' . count($actions) . " {$action} FINISH" . PHP_EOL . PHP_EOL;
        }

        $this->actionInfo();

        return ExitCode::OK;
    }

}