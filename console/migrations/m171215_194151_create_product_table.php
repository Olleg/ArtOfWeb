<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product`.
 */
class m171215_194151_create_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%product}}', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer(),
            'type' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'price' => $this->integer()->notNull(),
            'image' => $this->string(),
        ]);
        $this->createIndex('IDX_product_category_id', '{{%product}}', 'category_id');
        $this->addForeignKey('FK_product_category_id', '{{%product}}', 'category_id', '{{%category}}', 'id', 'SET NULL', 'RESTRICT');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%product}}');
    }
}
