<?php

namespace common\models;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\common\models\Category]].
 *
 * @see \common\models\Category
 */
class CategoryQuery extends ActiveQuery
{
    /**
     * @return self
     */
    public function root()
    {
        return $this->andWhere(['parent_id' => null]);
    }

    /**
     * @return self
     */
    public function category($id)
    {
        return $this->andWhere(['parent_id' => $id]);
    }

    /**
     * @inheritdoc
     * @return \common\models\Category[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\Category|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }


}