<?php

namespace common\models;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\common\models\Product]].
 *
 * @see \common\models\Product
 */
class ProductQuery extends ActiveQuery
{
    /**
     * @param $id
     * @param bool $includeChildrenCategory
     * @return $this
     */
    public function forCategory($id, $includeChildrenCategory = false)
    {
        $ids = [$id];
        if ($includeChildrenCategory) {
            $childrenIds = [$id];
            while ($childrenIds = Category::find()->select('id')->andWhere(['parent_id' => $childrenIds])->column()) {
                $ids = array_merge($ids, $childrenIds);
            }
        }

        return $this->andWhere(['category_id' => array_unique($ids)]);
    }

    /**
     * @inheritdoc
     * @return \common\models\Product[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\Product|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}