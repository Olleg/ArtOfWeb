<?php

namespace common\models;

use common\components\ActiveRecord;
use yii\helpers\ArrayHelper;
use yiidreamteam\upload\ImageUploadBehavior;

/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property integer $category_id
 * @property integer $type
 * @property string $name
 * @property integer $price
 * @property string $image
 * @property boolean $test
 *
 * @property Category $category
 */
class Product extends ActiveRecord
{
    const TYPE_DEFAULT = 0;
    const TYPE_NEW = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'type', 'price'], 'integer'],
            [['type', 'name', 'price'], 'required'],
            [['name'], 'string', 'max' => 255],
            ['image', 'file', 'extensions' => 'jpeg, gif, png, jpg'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => ImageUploadBehavior::class,
                'attribute' => 'image',
                'thumbs' => [
                    'thumb' => ['width' => 640, 'height' => 480],
                ],
                'filePath' => '@backend/web/images/[[model]]/[[pk]].[[extension]]',
                'fileUrl' => '/images/[[model]]/[[pk]].[[extension]]',
                'thumbPath' => '@backend/web/images/[[model]]/[[profile]]_[[pk]].[[extension]]',
                'thumbUrl' => '/images/[[model]]/[[profile]]_[[pk]].[[extension]]',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Категория',
            'type' => 'Тип',
            'name' => 'Название',
            'price' => 'Стоимость',
            'image' => 'Изображение',
        ];
    }

    /**
     * @return array
     */
    public static function typeLabels()
    {
        return [
            self::TYPE_DEFAULT => 'По умолчанию',
            self::TYPE_NEW => 'Новый',
        ];
    }

    /**
     * @param null $default
     * @return mixed
     */
    public function typeLabel($default = null)
    {
        return ArrayHelper::getValue(static::typeLabels(), $this->type, $default);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @inheritdoc
     * @return ProductQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProductQuery(get_called_class());
    }

    /**
     * @return bool
     */
    public function isNew()
    {
        return $this->type == self::TYPE_NEW;
    }
}
