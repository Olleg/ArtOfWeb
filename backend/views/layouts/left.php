
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->identity->profile->fullName ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <?php if (false): ?>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <?php endif; ?>


        <?= \dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Магазин', 'options' => ['class' => 'header']],
                    ['label' => 'Категории', 'icon' => 'folder', 'url' => ['/shop/category']],
                    ['label' => 'Товары', 'icon' => 'star', 'url' => ['/shop/product']],

                    ['label' => 'Главное меню', 'options' => ['class' => 'header']],
                    ['label' => 'Изменить пароль', 'icon' => 'lock', 'url' => ['/profile/change-password']],
                ],
            ]
        ) ?>

    </section>

</aside>
