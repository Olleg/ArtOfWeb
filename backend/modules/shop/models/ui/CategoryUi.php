<?php
/**
 * @author Antonov Oleg <theorder83dev@gmail.com>
 */

namespace backend\modules\shop\models\ui;


use common\models\Category;
use yii\helpers\ArrayHelper;

/**
 * Class CategoryUi
 * @package backend\modules\shop\models\ui
 */
class CategoryUi
{
    /**
     * @return array
     */
    public static function categoryList()
    {
        return ArrayHelper::map(Category::find()->orderBy('name')->all(), 'id', 'name');
    }



}
