<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form">

    <?php $form = ActiveForm::begin([
        'enableClientValidation' => false,
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'category_id')->dropDownList(
        \backend\modules\shop\models\ui\CategoryUi::categoryList(),
        ['prompt' => 'Выбор категории']
    ) ?>

    <?= $form->field($model, 'type')->dropDownList(\common\models\Product::typeLabels()) ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'image')->fileInput() ?>

    <?php if ($model->getImageFileUrl('image')): ?>
        <div class="row">
            <div class="col-xs-6">
                <div class="thumbnail">
                    <img src="<?= $model->getThumbFileUrl('image', 'thumb') ?>" alt="Nature" style="width:100%">
                </div>
            </div>
        </div>
    <?php endif; ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
