<?php
/**
 * @author Antonov Oleg <theorder83dev@gmail.com>
 */

namespace backend\modules\shop;

use yii\base\BootstrapInterface;

/**
 * Class Module
 * @package backend\modules\shop
 */
class Module extends \yii\base\Module implements BootstrapInterface
{
    public $controllerNamespace = '\backend\modules\shop\controllers';

    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules(
            require(__DIR__ . '/config/routes.php'),
            false
        );
    }
}
