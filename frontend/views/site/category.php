<?php

/* @var $this yii\web\View */
$this->title = $title;

?>

<?php if ($categories): ?>
    <div class="row">
        <div class="col s12">
            <h1> Подкатегории </h1>
            <div class="collection">
                <?php
                /** @var \common\models\Category $category */
                foreach ($categories as $category): ?>
                    <a href="/category/<?= $category->id ?>" class="collection-item">
                        <span class="badge"> Товаров: <?= \common\models\Product::find()->forCategory($category->id, true)->count(); ?></span>
                        <?= $category->name; ?>
                    </a>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php endif; ?>


<?php if ($products): ?>
    <div class="row">
        <div class="col s12">
            <h1> Товары </h1>
            <div class="row">

                <?php
                /** @var \common\models\Product $products */
                foreach ($products as $product): ?>
                    <div class="col s4">
                        <div class="card">
                            <div class="card-image">
                                <img src="<?= $product->getThumbFileUrl('image') ?>">
                            </div>
                            <div class="card-content">
                                <span class="card-title"> <?= $product->name; ?></span>

                                <a href="/product/<?= $product->id ?>" class="btn">
                                    Смотреть
                                </a>

                            </div>
                        </div>
                    </div>


                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php if (!$categories && !$products): ?>
    <div class="row">
        <div class="col s12">
            <div class="card-panel red lighten-4">Категория <?= $title ?> пуста </div>
        </div>
    </div>
<?php endif; ?>


<div class="row">
    <div class="col s12">
        <?php if ($parent_id): ?>
        <a class="btn orange" href="/category/<?= $parent_id ?>">
            <?php else: ?>
            <a class="btn orange" href="/">
                <?php endif; ?>
                <i class="fa fa-reply"></i> Назад
            </a>
    </div>
</div>



