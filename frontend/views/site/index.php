<?php

/* @var $this yii\web\View */
$this->title = $title;
?>
<div class="row">
    <div class="col s12">
        <h2> Категории </h2>
        <div class="collection">
            <?php
            /** @var \common\models\Category $category */
            foreach ($categories as $category): ?>
                <a href="/category/<?= $category->id ?>" class="collection-item">
                    <span class="badge"> Товаров: <?= \common\models\Product::find()->forCategory($category->id, true)->count(); ?></span>
                    <?= $category->name; ?>
                </a>
            <?php endforeach; ?>
        </div>
        <?php if (!$categories): ?>
            <div class="card-panel red lighten-4">Нет категорий</div>
        <?php endif; ?>
    </div>
</div>

