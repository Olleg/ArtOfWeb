<?php
/** @var \common\models\Product $product */
/* @var $this yii\web\View */
$this->title = $product->name;

?>


<div class="row">
    <div class="col s12">
        <div class="container">
            <h1 class="text-cent">
                <?= $product->name ?>
            </h1>
            <div class="something">
                <img class="responsive-img" style="width: 100%" src="<?= $product->getImageFileUrl('image') ?>">
                <?php if ($product->isNew()): ?>
                    <div class="arrow-right">
                        <span>New</span>
                    </div>
                <?php endif; ?>
            </div>
            <h1 class="text-cent">Цена: <?= $product->price ?> <i class="fa fa-euro"></i></h1>
            <a class="btn orange" href="/category/<?= $product->category_id ?>">
                <i class="fa fa-reply"></i> Назад
            </a>
        </div>
    </div>
</div>


