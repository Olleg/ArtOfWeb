<?php

namespace frontend\controllers;

use common\models\Category;
use common\models\Product;
use yii\web\Controller;
use yii\web\NotFoundHttpException;


/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $categories = Category::find()->root()->all();

        return $this->render('index',
            [
                'title' => 'Каталог',
                'categories' => $categories,
            ]
        );
    }

    /**
     * @param $id
     * @return string
     */
    public function actionCategory($id)
    {
        $categories = Category::find()->category($id)->all();
        $currentCategory = Category::findOne($id);
        $products = Product::find()->forCategory($id)->all();
        return $this->render('category',
            [
                'title' => $currentCategory->name,
                'parent_id' => $currentCategory->parent_id,
                'categories' => $categories,
                'products' => $products,
            ]
        );
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionProduct($id)
    {
        if (!$product = Product::findOne($id)){
            throw new NotFoundHttpException('Product not found');
        }
        return $this->render('product',
            [
                'product' => $product,
            ]
        );
    }


}
